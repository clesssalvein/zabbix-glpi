#!/bin/bash

# $1 - destination
# $2 - topic
# $3 - message

echo=`which echo`
awk=`which awk`
sed=`which sed`
mysql=`which mysql`

# glpi api
glpiApiPath="http://<GLPI_IP_OR_DOMAIN_NAME>/apirest.php"
userToken="<GLPI_USER_API_TOKEN>"
appToken="<GLPI_APP_TOKEN>"

# zabbix alert vars
ticketName=${2}
ticketMessage=${3}

# get session_token
sessionToken=`curl -s -X GET \
-H 'Content-Type: application/json' \
-H "Authorization: user_token ${userToken}" \
-H "App-Token: ${appToken}" \
"${glpiApiPath}/initSession" \
| jq -r '.session_token'`;

# parse eventId from message
eventId=`${echo} -e ${ticketMessage} | ${awk} '/EventID/{print $2}'`

# parse eventSeverity from message
eventSeverity=`${echo} -e ${ticketMessage} | ${awk} -F": " '/Приоритет заявки/{print $2}'`

# because in zabbix the severity of the problem is numbered 0-5, and in the glpi priority numbered 1-6, then we add to the zabbix severity +1
eventSeverity=`echo $[${eventSeverity} + 1]`

# parse from message "ID of the organization in GLPI" (in zabbix it is recorded manually in the "Tag" field in the inventory of every host)
entityId=`${echo} -e ${ticketMessage} | ${awk} -F": " '/ID отдела организации в GLPI/{print $2}'`

# parse TriggerID from message
triggerId=`${echo} -e ${ticketMessage} | ${awk} -F": " '/TriggerID/{print $2}'`


# FINDING ALREADY CREATED TICKETS WITH THE SAME triggerId IN GLPI AS IN NEW EVENT OF ZABBIX,
# ADD THE FOLLOWUP IN THE ALREADY OPEN TICKETS WITH THAT triggerId

# finding for ticket IDs with the same TriggerID as in new trigger, with the status NOT closed, put them in the array arrayOpenTicketIds
arrayOpenTicketIds=( $(curl -s -X GET -H 'Content-Type: application/json' -H "Session-Token: ${sessionToken}" -H "App-Token: ${appToken}" "${glpiApiPath}/Ticket?searchText\[content\]=TriggerID:%20${triggerId}&searchText\[status\]=[1-5]" | jq -r '.[]' | jq -r '.id') );

# if array arrayOpenTicketIds NOT empty
if ! [[ ${#arrayOpenTicketIds[@]} -eq 0 ]]; then

    # for every ticketID from array add followup in ticket with that ticketID
    for openedTicketId in "${arrayOpenTicketIds[@]}";
    do
        curl -s -X POST -H 'Content-Type: application/json' -H "Session-Token: ${sessionToken}" -H "App-Token: ${appToken}" -d '{"input": {"tickets_id": "'"${openedTicketId}"'", "content": "'"${ticketMessage}"'"}}' "${glpiApiPath}/Ticket/${openedTicketId}/TicketFollowup"
        echo "$openedTicketId";
    done;

else

    # IF TICKETS WITH THIS triggerId DONT'T EXIST - ADD NEW TICKET
    
    # add new ticket
    curl -s -X POST -H 'Content-Type: application/json' -H "Session-Token: ${sessionToken}" -H "App-Token: ${appToken}" -d '{"input": {"entities_id": "'"${entityId}"'","name": "'"${ticketName}"'","content": "'"${ticketMessage}"'","status": "2","priority": "'"${eventSeverity}"'"}}' "${glpiApiPath}/Ticket/"

fi

# kill session
curl -s -X GET \
-H 'Content-Type: application/json' \
-H "Session-Token: ${sessionToken}" \
-H "App-Token: ${appToken}" \
"${glpiApiPath}/killSession";
